﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PuckControl : MonoBehaviour {

	public Transform startingPos;
	private Rigidbody rigidbody;
	public AudioClip wallCollideClip;
	public AudioClip paddleCollideClip;
	public LayerMask paddleLayer;
	private AudioSource audio;

	void Start () {
		audio = GetComponent<AudioSource> ();
		rigidbody = GetComponent<Rigidbody> ();
		ResetPosition();
	}



	void OnCollisionEnter(Collision collision) {
		// check what we have hit
		if (paddleLayer.Contains(collision.gameObject)) {
			// hit the paddle
			audio.PlayOneShot(paddleCollideClip);
		}
		else {
			// hit something else
			audio.PlayOneShot(wallCollideClip);
		}
	}


	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionStay(Collision collision) {
		Debug.Log("Collision Stay");
	}

	void OnCollisionExit(Collision collision) {
		Debug.Log("Collision Exit");
	}
	public void ResetPosition() {
		rigidbody.MovePosition(startingPos.position);
		rigidbody.velocity = Vector3.zero;
	}

}
