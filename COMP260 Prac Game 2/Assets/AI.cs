﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class AI : MonoBehaviour {
	private Rigidbody rigidbody;
	public Transform target;
	private Transform puck;


	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;
		PuckControl p = FindObjectOfType<PuckControl> ();
		puck = p.transform;

	}
	private Vector3 GetMousePosition() {

		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);


		Plane plane = new Plane(Vector3.forward, Vector3.zero);
		float distance = 0;
		plane.Raycast(ray, out distance);
		return ray.GetPoint(distance);
	}
	void OnDrawGizmos() {

		Gizmos.color = Color.yellow;
		Vector3 pos = GetMousePosition();
		Gizmos.DrawLine(Camera.main.transform.position, pos);
	}



	public float speed = 20f; 
	void FixedUpdate () {
		
		target = puck;
	

		Vector2 direction = target.position - transform.position;
		Vector2 vel = direction * speed;

		rigidbody.velocity = vel;
	}
		
	public float maxSpeed = 5.0f;
	void Update () {

	}
}
