﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class MovePaddle : MonoBehaviour {
	private Rigidbody rigidbody;


	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;

	}
	private Vector3 GetMousePosition() {

		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);


		Plane plane = new Plane(Vector3.forward, Vector3.zero);
		float distance = 0;
		plane.Raycast(ray, out distance);
		return ray.GetPoint(distance);
	}
	void OnDrawGizmos() {

		Gizmos.color = Color.yellow;
		Vector3 pos = GetMousePosition();
		Gizmos.DrawLine(Camera.main.transform.position, pos);
	}



	public float speed = 20f; 
	void FixedUpdate () {

		rigidbody.velocity = direction*maxSpeed;
	}

	private Vector3 direction;
	public float maxSpeed = 5.0f;
	void Update () {
		direction.x = Input.GetAxis("Horizontal");
		direction.y = Input.GetAxis("Vertical");
	}
}
