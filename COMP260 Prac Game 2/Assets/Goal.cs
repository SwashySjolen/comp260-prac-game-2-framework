﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Goal : MonoBehaviour {
	
public AudioClip scoreClip;
	public AudioSource audio;

	void Start() {
		audio = GetComponent<AudioSource> ();
	}

	void OnTriggerEnter(Collider collider) {
		audio.PlayOneShot (scoreClip);
		PuckControl puck = 
			collider.gameObject.GetComponent<PuckControl> ();
		puck.ResetPosition();
	}

	// Update is called once per frame

	void Update () {
		
	}
}
